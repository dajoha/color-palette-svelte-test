export interface Rgb {
    r: number,
    g: number,
    b: number,
}

export interface Hsl {
    h: number,
    s: number,
    l: number,
}

export class Color {
    public hex: string;

    public h: number = 0.0;
    public s: number = 0.0;
    public l: number = 0.0;

    constructor(
        public r: number = 0,
        public g: number = 0,
        public b: number = 0,
    ) {
        this.updateFromRgb();
    }

    public static fromRgb({r, g, b}: Rgb): Color {
        return new Color(r, g, b);
    }

    public static fromHsl(hsl: Hsl): Color {
        return Color.fromRgb(hslToRgb(hsl));
    }

    public updateFromRgb() {
        this.setHsl(rgbToHsl(this));
        this.updateHex();
    }

    public updateFromHsl() {
        this.setRgb(hslToRgb(this));
        this.updateHex();
    }

    public updateFromHex(): boolean {
        let rgb = parseHexColor(this.hex);
        if (rgb === null) {
            return false;
        }

        this.setRgb(rgb);
        this.setHsl(rgbToHsl(this));
        return true;
    }

    protected setHsl(hsl: Hsl) {
        this.h = hsl.h;
        this.s = hsl.s;
        this.l = hsl.l;
    }

    protected setRgb(rgb: Rgb) {
        this.r = rgb.r;
        this.g = rgb.g;
        this.b = rgb.b;
    }

    protected updateHex() {
        this.hex = '#';
        for (let c of [this.r, this.g, this.b]) {
            c = Math.round(c);
            this.hex += ('00' + c.toString(16)).slice(-2);
        }
    }
}

export function parseHexColor(hex: string): Rgb|null {
    if (hex[0] === '#') {
        hex = hex.slice(1);
    }
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    } else if (hex.length !== 6) {
        return null;
    }
    let rgbComponents = [];
    for (let i = 0; i !== 3; i++) {
        const component = parseInt(hex.slice(i * 2, i * 2 + 2), 16);
        if (isNaN(component)) {
            return null;
        }
        rgbComponents.push(component);
    }
    let [ r, g, b ] = rgbComponents;
    return { r, g, b };
}

/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * Adapted from https://gist.github.com/mjackson/5311256
 */
export function rgbToHsl({r, g, b}: Rgb): Hsl {
    r /= 255;
    g /= 255;
    b /= 255;

    const max = Math.max(r, g, b), min = Math.min(r, g, b);
    let h, s, l = (max + min) / 2;

    if (max == min) {
        h = s = 0; // achromatic
    } else {
        let d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }

        h /= 6;
    }

    return { h, s, l };
}

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * Adapted from https://gist.github.com/mjackson/5311256
 */
export function hslToRgb({h, s, l}: Hsl): Rgb {
    let r: number, g: number, b: number;

    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1/6) return p + (q - p) * 6 * t;
            if (t < 1/2) return q;
            if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        const p = 2 * l - q;

        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return { r: r * 255, g: g * 255, b: b * 255 };
}
